## cmt
### your local git statistics
cmt crawls your home folder or pwd for git repos and pulls statistics.

other pretty options to come, like charts and graphs oh my.

### installation
`go get -u rjl.li/cmt`

### usage
type `cmt` for default usage. crawl your home directory for git repos and pull stats.

`cmt -s` to include you current streak and longest from the last year.

### features of the future
- all version control systems
- date ranges
- single repos
- output svg or png