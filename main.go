package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	path "path/filepath"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"
)

type repo struct {
	name    string
	commits []int64
}

var wg sync.WaitGroup
var repos = make(chan repo)

var stats struct {
	globalcommits [][]int64
	streak        int
}

// dates provides a custom type for unix time
type dates []int64

// with methods that allow sorting with pkg sort
func (a dates) Len() int           { return len(a) }
func (a dates) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a dates) Less(i, j int) bool { return a[i] < a[j] }

var streak = flag.Bool("s", false, "include global commit streak")

var tokens = make(chan struct{}, 30)

func main() {
	flag.Parse()
	// start in users home directory
	start := os.Getenv("HOME")
	if len(start) < 1 {
		// if env var not set, use working dir
		var err error
		start, err = os.Getwd()
		if err != nil {
			fmt.Fprintf(os.Stderr, "error getting working directory: %v\n", err)
			os.Exit(1)
		}
	}

	wg.Add(1)
	go crawl(start)
	go func() {
		wg.Wait()
		close(repos)
	}()

	for r := range repos {
		if len(r.commits) > 0 {
			stats.globalcommits = append(stats.globalcommits, r.commits)
		}
	}
	var total int
	count := len(stats.globalcommits)
	for _, r := range stats.globalcommits {
		total += len(r)
	}

	if *streak {
		// get number of streak days and commits
		sd, sc, lsd, lsc := sortandcount()
		fmt.Printf("Your current streak is %d days, for a total of %d commits.\nYour longest streak this year was %d days with %d commits.\nIn the last year you made %d commits across %d repositories.\n", sd, sc, lsd, lsc, total, count)
	} else {
		// just get total commits and repos
		fmt.Printf("You made %d commits across %d repositories in the last year.\n", total, count)
	}
}

// crawl scans the directory s and crawls subsequent directories
// ignoring hidden folders and calling the vcs function for any
// repositories found.
func crawl(s string) {
	tokens <- struct{}{}
	dir, err := os.Open(s)
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed opening directory %s: %v\n", s, err)
	}
	dirs, err := dir.Readdirnames(0)
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed reading directories in %s: %v\n", dir.Name(), err)
	}
	dir.Close()
	for _, d := range dirs {
		full := path.Join(s, d)
		// full := s + "/" + d
		f, err := os.Stat(full)
		if err != nil {
			fmt.Fprintf(os.Stderr, "could not get file stats: %v\n", err)
		}
		if f.IsDir() {
			switch d {
			case ".git":
				wg.Add(1)
				go git(d, full)
			default:
				// only non hidden directories
				if d[0] != '.' {
					wg.Add(1)
					go crawl(full)
				}
			}
		}
	}
	wg.Done()
	<-tokens
}

// git parses a git repository for commits
func git(n, p string) {
	tokens <- struct{}{}
	defer wg.Done()

	log, err := os.Open(p + "/logs/HEAD")
	defer log.Close()
	if err != nil {
		// coudln't open HEAD, skip this repo
		return
	}

	r := repo{
		name: n,
	}
	scanner := bufio.NewScanner(log)
	for scanner.Scan() {
		line := scanner.Text()
		info := strings.Fields(line)
		// 0				1		2		3			4			 5		 6			 7			 8
		// previous id, id, user, email, time, offset, action, comments
		d, err := strconv.Atoi(info[4])
		if err != nil {
			fmt.Fprintf(os.Stderr, "could not convert time to int: %v\n", err)
		}
		date := int64(d)
		// only log commit events, max one year old
		// TODO make this time span customizable
		if info[6] == "commit:" && time.Since(time.Unix(date, 0)) <= time.Duration(8760)*time.Hour {
			r.commits = append(r.commits, date)
		}
	}
	repos <- r
	<-tokens
}

// sortandcount combines and sorts the slices of commit dates
// then totals the current and longest running streaks
func sortandcount() (sd, sc, lsd, lsc int) {
	// merge and sort globalcommits in descending order
	// TODO use actual merge sort for speed
	var allcommits dates
	for _, gc := range stats.globalcommits {
		for _, c := range gc {
			allcommits = append(allcommits, c)
		}
	}
	sort.Sort(sort.Reverse(allcommits))
	var days, commits, currstreak, currstreakcommits, longstreakdays, longstreakcommits int
	var lastdate time.Time
	// iterate over sorted commits
	for i, d := range allcommits {
		// on first iteration the last date is the first in the list
		currdate := time.Unix(d, 0)
		if i == 0 {
			lastdate = currdate
			// if that date is also today, we start counting the streak
			if sameday(currdate, time.Now()) {
				days++
				commits++
			}
		} else {
			// if still on last day and more commits, increase commits this streak
			if sameday(currdate, lastdate) {
				commits++
			} else if nextday(currdate, lastdate) {
				lastdate = currdate
				days++
				commits++
			} else {
				if currstreak == 0 {
					currstreak, longstreakdays = days, days
					currstreakcommits, longstreakcommits = commits, commits
				} else {
					if longstreakdays < days {
						longstreakdays = days
						longstreakcommits = commits
					}
					days, commits = 0, 0
				}
				lastdate = currdate
			}
		}
	}
	return currstreak, currstreakcommits, longstreakdays, longstreakcommits
}

// sameday returns a bool if the dates are the same day
func sameday(a, b time.Time) bool {
	return a.Month() == b.Month() && a.Day() == b.Day()
}

func nextday(a, b time.Time) bool {
	// if dates do not match
	// but are within one day, i.e. jan 31 and feb 1
	tomorrow := a.AddDate(0, 0, 1)
	if sameday(tomorrow, b) {
		return true
	}
	return false
}
